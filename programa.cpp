#include <iostream>
#include "Pila.h"
using namespace std;

void pila_vacia(int tope, bool &band){
  
  if (tope == -1){ 
    band = true;
  }
  else{
    band = false;
  }
}

void pila_llena(int tope, int tamano, bool &band){
  
  if (tope == tamano){
    band = true;
  }
  else{
    band = false;
  }
}

void push(int pila[], int tope, int tamano, int &numero, bool &band){
  
  pila_llena(tope, tamano, band);
  if(band == false){
    tope = tope + 1;
    pila[tope]=numero;
    cout << pila[tope] << endl;
  }
  else{
    cout << "Desbordamiento, pila llena" << endl;
  }
  cout << pila[tope] << endl;
}
void pop(int pila[], int tope){
  
  if (tope <= -1){
    cout << "Subdesbordamiento, Pila vacı́a"<< endl;
  }
  else{
    cout << "Elemento eliminado: " << pila[tope] << endl;
    tope--;
  }
}

void ver_pila(int pila[], int tope, bool &band){
  cout << band << endl;
  if(tope >= 0){
    for (int i=tope; i>=-1; i--){
      cout << "|" << pila[i] << "|" << endl;
    }
  }
  else{
    cout << "La pila esta vacia" << endl;
  }
}

void menu(int pila[], int tamano, int tope){
  int opcion;
  int numero;
  bool band;
  do {
    cout << "---------------------------" << endl;
    cout << "1. Agregar/push" << endl;
    cout << "2. Remover/pop" << endl;
    cout << "3. Ver pila" << endl;
    cout << "4. salir" << endl;
    cout << "---------------------------" << endl;
    
    cout << "\nIngrese una opcion: ";
    cin >> opcion;
    
    switch (opcion) {
    case 1:
      cout << "Ingrese el numero a agregar: " << endl;
      cin >> numero;
      push(pila, tope, tamano, numero, band);
      cout << pila[tope] << endl;
      break;
      
    case 2:
      pop(pila, tope);
      break;
      
    case 3:
      ver_pila(pila, tope, band);
      break;
      
    }        
  } while (opcion != 4);
}

int main() {
  int tamano;
  //Se ingresa el tamaño de la pila
  cout << "Ingrese el tamaño: " << endl;
  cin >> tamano;
  if (tamano > 0){
    int tope = -1;
    int pila[tamano];
    menu(pila, tope, tamano);
  }
  return 0;
}




